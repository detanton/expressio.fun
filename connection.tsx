import mongoose from 'mongoose';

const connection = async () => {
    const params = {
        useNewUrlParser: true,
        useUnifiedTopology: true
    };

    try {
        const db = await mongoose.connect('mongodb://localhost:27017/expressio', params);

        return { db };
    } catch (error) {
        console.log(error);
    }
};

export default connection;