import React, { createRef, FC } from 'react';
import { Container, Sticky } from 'semantic-ui-react';
import Navigation from '../components/navigation';
import TopNavigation from '../components/top-navigation';

const MainLayout: FC = ({ children }) => {
    const contextRef = createRef<HTMLDivElement>();

    return (
        <div ref={contextRef}>
            <Container>
                <header>
                    <nav className="top-navigation">
                        <Sticky context={contextRef}>
                            <TopNavigation />
                        </Sticky>
                    </nav>
                </header>
                <aside>
                    <nav className="aside-navigation">
                        <Navigation />
                    </nav>
                </aside>
                <main>
                    <Container>
                        {children}
                    </Container>
                </main>
                <style jsx>{`
                    .aside-navigation {
                        position: fixed;
                        margin-top: 1rem;
                    }
                    main {
                        margin-left: 15rem;
                        padding: 1rem;
                        padding-right: 0;
                    }
                `}</style>
            </Container>
        </div>
    );
};
  
export default MainLayout;