import Link from 'next/link';
import React, { createRef, FC } from 'react';
import { Container, Icon, Sticky } from 'semantic-ui-react';

const PostLayout: FC = ({ children }) => {
    const contextRef = createRef<HTMLDivElement>();

    return (
        <div ref={contextRef}>
            <Container>
                <header>
                    <nav className="top-navigation">
                        <Sticky context={contextRef}>
                            <div className="back-to-posts">
                                <Link href="/" passHref>
                                    <a>
                                        <Icon name="long arrow alternate left" size="big" />
                                        <span>Вернуться к новостям</span>
                                    </a>
                                </Link>
                            </div>
                        </Sticky>
                    </nav>
                </header>
                <main>
                    <Container>
                        {children}
                    </Container>
                </main>
            </Container>
            <style jsx>{`
                .back-to-posts {
                    background-color: #f1f1f1;
                    padding: 1rem;
                }
            `}</style>
        </div>
    );
};
  
export default PostLayout;