import User from '../components/user';
import MainLayout from '../layouts/main';
import connection from '../connection';

const Profile = ({
    posts
}) => (
    <MainLayout>
        <User posts={posts} />
    </MainLayout>
);

export const getServerSideProps = async () => {
    const { db } = await connection();

    const Posts = await db.models.Posts || await db.model('Posts', { header: String, content: Array });
    const posts = await Posts.find({});
    const preparedNews = JSON.parse(JSON.stringify(posts));

    return {
        props: {
            posts: preparedNews
        }
    };
};

export default Profile;