import React, { FC } from 'react';
import FriendsList from '../components/friends-list';
import MainLayout from '../layouts/main';

const Friends: FC = () => (
    <MainLayout>
        <FriendsList />
    </MainLayout>
);
  
export default Friends;