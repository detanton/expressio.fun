import { AppProps } from 'next/app';
import 'semantic-ui-css/semantic.min.css';

import '../public/css/main.css';

const ExperessioFun = ({
    Component,
    pageProps
}: AppProps) => (
    <Component {...pageProps} />
);

export default ExperessioFun;