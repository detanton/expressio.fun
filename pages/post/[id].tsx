import React, { FC } from 'react';
import { Grid, Header, Icon, Segment, Image, Container } from 'semantic-ui-react';
import PostLayout from '../../layouts/post';
import connection from '../../connection';
import { GetServerSideProps } from 'next';

interface Post {
    post: {
        _id?: string,
        header: string,
        community?: string,
        content: string[]
    }
}

const Post: FC<Post> = ({
    post
}) => (
    <PostLayout>
        <Container>
            <Segment vertical>
                <Header as='h3' color="grey">
                    <Icon name='newspaper' />
                    <Header.Content>
                        {post.header}
                        <Header.Subheader>
                        Сообщество &ldquo;Взгляд&rdquo;
                        </Header.Subheader>
                    </Header.Content>
                </Header>
            </Segment>
            <Segment vertical size="large">
                {post?.content.map((item) => {
                    if (item.search(/image/) === 0) {
                        const arr = item.split(';');
                        const [type, src, alt] = arr;

                        return (
                            <Image key={src} src={src} alt={alt} fluid />
                        );
                    }

                    return (
                        <p key={item}>{item}</p>
                    );
                })}
            </Segment>
            <Segment vertical>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={2}>
                            <Icon name="heart" />
                            <span>23</span>
                        </Grid.Column>
                        <Grid.Column width={2}>
                            <Icon name="comment" />
                            <span>9</span>
                        </Grid.Column>
                        <Grid.Column width={2}>
                            <Icon name="retweet" />
                            <span>2</span>
                        </Grid.Column>
                        <Grid.Column floated="right" width={2}>
                            <Icon name="eye" />
                            <span>236</span>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        </Container>
    </PostLayout>
);

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const { params: { id } } = ctx;
    const { db } = await connection();

    const Posts = await db.models.Posts || await db.model('Posts', { header: String, content: Array });
    const posts = await Posts.findOne({ _id: id });
    const preparedNews = JSON.parse(JSON.stringify(posts));

    return {
        props: {
            post: preparedNews
        }
    };
};

export default Post;