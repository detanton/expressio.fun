import React, { FC } from 'react';
import Messages from '../components/messages';
import MainLayout from '../layouts/main';

const Posts: FC = () => (
    <MainLayout>
        <Messages />
    </MainLayout>
);
  
export default Posts;