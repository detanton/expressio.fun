import React, { FC } from 'react';
import NewsFeed from '../components/news-feed';
import MainLayout from '../layouts/main';
import connection from '../connection';

const HomePage: FC = ({
    posts
}) => (
    <MainLayout>
        <NewsFeed posts={posts} />
    </MainLayout>
);

export const getServerSideProps = async () => {
    const { db } = await connection();

    const Posts = await db.models.Posts || await db.model('Posts', { header: String, content: Array });
    const posts = await Posts.find({});
    const preparedNews = JSON.parse(JSON.stringify(posts));

    return {
        props: {
            posts: preparedNews
        }
    };
};
  
export default HomePage;