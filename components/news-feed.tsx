import React, { FC } from 'react';
import { Grid } from 'semantic-ui-react';
import Post from './post';

const NewsFeed: FC = ({
    posts
}) => (
    <Grid>
        <Grid.Column>
            <Grid.Row>
                {posts.map((post) => (
                    <Post key={post._id} {...post} />
                ))}
            </Grid.Row>
        </Grid.Column>
    </Grid>
);

export default NewsFeed;