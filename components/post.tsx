import Link from 'next/link';
import React, { FC } from 'react';
import { Grid, Header, Icon, Segment, Image } from 'semantic-ui-react';

interface Post {
    _id?: string,
    header: string,
    community?: string,
    content: string[]
}

const Post: FC<Post> = ({
    _id,
    header,
    content
}) => (
    <Segment raised padded>
        <Segment vertical>
            <Header as='h3' color="blue">
                <Icon name='newspaper' />
                <Header.Content>
                    <Link href={`/post/${_id}`}><a>{header}</a></Link>
                    <Header.Subheader>
                        Сообщество &ldquo;Взгляд&rdquo;
                    </Header.Subheader>
                </Header.Content>
            </Header>
        </Segment>
        <Segment vertical size="large">
            {content.map((item) => {
                if (item.search(/image/) === 0) {
                    const arr = item.split(';');
                    const [type, src, alt] = arr;

                    return (
                        <Image key={src} src={src} alt={alt} fluid />
                    );
                }

                return (
                    <p key={item}>{item}</p>
                );
            })}
        </Segment>
        <Segment vertical>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={2}>
                        <Icon name="heart" />
                        <span>23</span>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <Icon name="comment" />
                        <span>9</span>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <Icon name="retweet" />
                        <span>2</span>
                    </Grid.Column>
                    <Grid.Column floated="right" width={2}>
                        <Icon name="eye" />
                        <span>236</span>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    </Segment>
);

export default Post;