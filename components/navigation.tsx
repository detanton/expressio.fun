import React, { FC, useCallback, useState } from 'react';
import { Icon, Label, Menu } from 'semantic-ui-react';
import Link  from 'next/link';
import { useRouter }  from 'next/router';

const Navigation: FC = () => {
    const router = useRouter();
    const [activeItem, setActiveItem] = useState(router.pathname);

    const handleClick = useCallback(() => {
        setActiveItem(router.pathname);
    }, [router.pathname]);

    return (
        <Menu vertical>
            <Link href="/" passHref>
                <Menu.Item
                    name="Новости"
                    color="blue"
                    active={activeItem === '/'}
                    onClick={handleClick}
                >
                    {'Новости'}
                    <Icon name="newspaper" />
                </Menu.Item>
            </Link>
            <Link href="/messages" passHref>
                <Menu.Item
                    name="Сообщения"
                    color="blue"
                    active={activeItem === '/messages'}
                    onClick={handleClick}
                >
                    {'Сообщения'}
                    <Icon name="envelope" />
                    <Label color="teal">+22</Label>
                </Menu.Item>
            </Link>
            <Link href="/friends" passHref>
                <Menu.Item
                    name="Друзья"
                    color="blue"
                    active={activeItem === '/friends'}
                    onClick={handleClick}
                >
                    {'Друзья'}
                    <Icon name="users" />
                    <Label color="teal">+1</Label>
                </Menu.Item>
            </Link>
        </Menu>
    );
};

export default Navigation;