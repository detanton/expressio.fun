import React, { FC } from 'react';
import { Feed, Segment } from 'semantic-ui-react';

const Messages: FC = () => (
    <>
        <Segment padded raised color="green">
            <Feed>
                <Feed.Event>
                    <Feed.Label image="https://react.semantic-ui.com/images/avatar/small/laura.jpg" />
                    <Feed.Content>
                        <Feed.Date content={new Date().toUTCString()} />
                        <Feed.Extra text content="Выражать себя и творить максимально естественным образом" />
                    </Feed.Content>
                </Feed.Event>
            </Feed>
        </Segment>
       
        <Segment padded>
            <Feed>
                <Feed.Event>
                    <Feed.Label image="https://react.semantic-ui.com/images/avatar/small/laura.jpg" />
                    <Feed.Content>
                        <Feed.Date content={new Date().toUTCString()} />
                        <Feed.Extra text content="Выражать себя и творить максимально естественным образом" />
                    </Feed.Content>
                </Feed.Event>
            </Feed>
        </Segment>

        <Segment padded>
            <Feed>
                <Feed.Event>
                    <Feed.Label image="https://react.semantic-ui.com/images/avatar/small/laura.jpg" />
                    <Feed.Content>
                        <Feed.Date content={new Date().toUTCString()} />
                        <Feed.Extra text content="Выражать себя и творить максимально естественным образом" />
                    </Feed.Content>
                </Feed.Event>
            </Feed>
        </Segment>
    </>
);

export default Messages;