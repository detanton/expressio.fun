import Link from 'next/link';
import React, { FC } from 'react';
import { Feed, Segment } from 'semantic-ui-react';

const FriendsList: FC = () => (
    <Segment padded>
        <Feed>
            <Feed.Event>
                <Feed.Label image='https://react.semantic-ui.com/images/avatar/small/elliot.jpg' />
                <Feed.Content>
                    <Feed.Date content="Был 2 дня назад" />
                    <Link passHref href="/user?id=03598"><a>Шамиль Абросов</a></Link>
                </Feed.Content>
            </Feed.Event>
            <Feed.Event>
                <Feed.Label image='https://react.semantic-ui.com/images/avatar/small/elliot.jpg' />
                <Feed.Content>
                    <Feed.Date content="Был 1 час назад" />
                    <Link passHref href="/user?id=03598"><a>Дюмин Алексей</a></Link>
                </Feed.Content>
            </Feed.Event>
            <Feed.Event>
                <Feed.Label image='https://react.semantic-ui.com/images/avatar/small/elliot.jpg' />
                <Feed.Content>
                    <Feed.Date content="online" />
                    <Link passHref href="/user?id=5987"><a>Пресняков Владимир</a></Link>
                </Feed.Content>
            </Feed.Event>
            <Feed.Event>
                <Feed.Label image='https://react.semantic-ui.com/images/avatar/small/elliot.jpg' />
                <Feed.Content>
                    <Feed.Date content="Был 30 мин назад" />
                    <Link passHref href="/user?id=25987"><a>Сашок</a></Link>
                </Feed.Content>
            </Feed.Event>
        </Feed>
    </Segment>
);

export default FriendsList;