import Link from 'next/link';
import React from 'react';
import { Card, Divider, Grid, Image, List } from 'semantic-ui-react';
import Carousel from './carousel';
import Post from './post';

const User = ({
    posts
}) => (
    <>
        <Grid>
            <Grid.Row>
                <Grid.Column width={5}>
                    <Card>
                        <Image src="https://s3.envato.com/files/281561201/EX-collection.jpg" alt="" />
                        <Card.Content>
                            <Card.Header>Daniel Franklin</Card.Header>
                        </Card.Content>
                    </Card>
                </Grid.Column>
                <Grid.Column width={5}>
                    <Grid>
                        <Grid.Row>
                            <List verticalAlign="middle">
                                <List.Item>
                                    <List.Content>
                                        <List.Header>Страна:</List.Header>
                                        <List.Description>Бангладеш</List.Description>
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Content>
                                        <List.Header>Пол:</List.Header>
                                        <List.Description>Мужской</List.Description>
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Content>
                                        <List.Header>Родился:</List.Header>
                                        <List.Description>23 сентября 1989</List.Description>
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Content>
                                        <List.Header>Семейное положение:</List.Header>
                                        <List.Description>Женат</List.Description>
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Content>
                                        <List.Header>Друзья:</List.Header>
                                        <List.Description>123</List.Description>
                                    </List.Content>
                                </List.Item>
                            </List>
                        </Grid.Row>
                    </Grid>
                </Grid.Column>
                <Grid.Column width={6}>
                    <Grid.Row>
                        <List>
                            <List.Item>
                                <List.Content>
                                    <List.Header>Работает в:</List.Header>
                                    <List.Description>Google corp.</List.Description>
                                </List.Content>
                            </List.Item>
                            <List.Item>
                                <List.Content>
                                    <List.Header>Интересы:</List.Header>
                                    <List.Description>Категория «законный интерес» - весьма специфический феномен правовой системы современного общества, который достаточно сложен для его правильного, объективного и адекватного юридического восприятия.</List.Description>
                                </List.Content>
                            </List.Item>
                        </List>
                    </Grid.Row>
                </Grid.Column>
            </Grid.Row>
        </Grid>
        <Divider />
        <Grid container>
            <Grid.Row>
                <Carousel content={[
                    {
                        id: 0,
                        src: 'https://searchbiznes.ru/wp-content/uploads/2018/03/%D0%9C%D0%B5%D0%BB%D1%8C%D0%B1%D1%83%D1%80%D0%BD.jpg',
                        description: 'На отдыхе',
                        alt: ''
                    },
                    {
                        id: 1,
                        src: 'https://img11.postila.ru/data/15/81/9e/30/15819e30122d6aaa205348d77cb9e60ed8692edf3fe0b9aef080004f22181f21.jpg',
                        description: 'Анапа',
                        alt: ''
                    },
                    {
                        id: 2,
                        src: 'https://traveltimes.ru/wp-content/uploads/2021/05/%D0%BE%D1%82%D0%B4%D1%8B%D1%85-%D0%B2-%D0%9C%D0%B5%D0%BB%D1%8C%D0%B1%D1%83%D1%80%D0%BD%D0%B5.jpg',
                        description: 'В турции',
                        alt: ''
                    },
                    {
                        id: 3,
                        src: 'https://kartinkin.com/uploads/posts/2021-03/1616112310_19-p-gollandiya-krasivie-foto-20.jpg',
                        description: 'Кайф!!!',
                        alt: ''
                    },
                    {
                        id: 4,
                        src: 'http://mtdata.ru/u3/photo2ED8/20512272537-0/original.jpg',
                        description: 'На отдыхе',
                        alt: ''
                    },
                    {
                        id: 5,
                        src: 'https://searchbiznes.ru/wp-content/uploads/2018/03/%D0%9C%D0%B5%D0%BB%D1%8C%D0%B1%D1%83%D1%80%D0%BD.jpg',
                        description: 'На отдыхе',
                        alt: ''
                    }
                ]} />
                <p>
                    <Link href="/user?id=123&photos=all"><a>Всего 16 фотографий</a></Link>
                </p>
            </Grid.Row>
        </Grid>
        <Divider />
        <Grid>
            <Grid.Column>
                <Grid.Row>
                    {posts.map((post) => (
                        <Post key={post._id} {...post} />
                    ))}
                </Grid.Row>
            </Grid.Column>
        </Grid>
    </>
);

export default User;