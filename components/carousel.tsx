import React, { FC, useCallback, useEffect, useState } from 'react';
import { Image } from 'semantic-ui-react';
import ImageModalBox from './image-modal-box';

type ImageType = {
    id: number,
    src: string,
    description?: string,
    alt: string | ''
}

type Props = {
    content: Array<ImageType>
}

interface WheelEventExtended extends WheelEvent {
    wheelDelta?: number
}

const Carousel: FC<Props> = ({
    content
}) => {
    const [count, setCount] = useState(0);
    const [isOpen, setOpen] = useState(false);

    const length = (content && Array.isArray(content)) && content.length - 1 || 0;

    const handleClickImage = useCallback((e) => {
        const { id } = e.target;

        setCount(Number(id));
        setOpen(true);
    }, [setCount, setOpen]);

    const handleClickPrev = useCallback(() => {
        setCount(count != 0 ? count - 1 : length);
    }, [count, length]);

    const handleClickNext = useCallback(() => {
        setCount(count < length ? count + 1 : 0);
    }, [count, length]);

    const handlePressKey = useCallback((e) => {
        if (e.key === 'ArrowLeft') {
            setCount(count != 0 ? count - 1 : length);
        }

        if (e.key === 'ArrowRight') {
            setCount(count < length ? count + 1 : 0);
        }
    }, [count, length]);

    useEffect(() => {
        document.onkeydown = (e) => {
            handlePressKey(e);
        };
    }, [handlePressKey]);

    useEffect(() => {
        document.onwheel = (e) => {
            const { wheelDelta }: WheelEventExtended = e;
            const isUp = wheelDelta && wheelDelta > 0;

            if (isUp) {
                handleClickPrev();
            } else {
                handleClickNext();
            }
        };
    }, [handleClickPrev, handleClickNext]);

    return (
        <section>
            <Image.Group>
                {content?.map((image) => (
                    <Image
                        key={image.id}
                        id={image.id}
                        src={image.src}
                        alt={image.alt}
                        width={296}
                        height={203}
                        onClick={handleClickImage}
                    />
                ))}
            </Image.Group>
            <ImageModalBox
                src={content[count].src}
                alt={content[count].alt}
                description={content[count].description}
                openModal={setOpen}
                open={isOpen}
            />
        </section>
    );
};

export default Carousel;