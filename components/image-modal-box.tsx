import React, { Dispatch, FC, SetStateAction } from 'react';
import { Modal, Image } from 'semantic-ui-react';

type Props = {
    src?: string,
    alt?: string,
    description?: string,
    open: boolean,
    openModal: Dispatch<SetStateAction<boolean>>
};

const ImageModalBox: FC<Props> = ({
    src,
    alt,
    description,
    open,
    openModal
}) => (
    <Modal
        onClose={() => openModal(false)}
        onOpen={() => openModal(true)}
        open={open}
        size="large"
    >
        <Modal.Header>Select a Photo</Modal.Header>
        <Modal.Content image>
            <Image src={src} alt={alt} width="80%" />
            <Modal.Description>
                {description}
            </Modal.Description>
        </Modal.Content>
    </Modal>
);

export default ImageModalBox;