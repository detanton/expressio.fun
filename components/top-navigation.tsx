import Link from 'next/link';
import React, { FC } from 'react';
import { Input, Menu, Dropdown } from 'semantic-ui-react';

const TopNavigation: FC = () => (
    <Menu>
        <Menu.Item header>
                Experssio.fun
        </Menu.Item>
        <Menu.Menu position='right'>
            <Menu.Item>
                <Input icon='search' placeholder='Поиск...' />
            </Menu.Item>
            <Dropdown text="Антон" pointing className='link item' item>
                <Dropdown.Menu>
                    <Dropdown.Item>
                        <Link href="/profile" passHref>
                            <a>Профиль</a>
                        </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                        <Link href="/logout" passHref>
                            <a>Выйти</a>
                        </Link>
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </Menu.Menu>
    </Menu>
);

export default TopNavigation;